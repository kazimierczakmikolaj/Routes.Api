﻿using FakeItEasy;
using Microsoft.AspNetCore.Mvc;
using Routes.Api.Controllers;
using Routes.Api.Infrastructure.Exceptions;
using Routes.Api.Infrastructure.Models;
using Routes.Api.Infrastructure.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Routes.Api.Tests
{
    public class RoutesControllerTests
    {
        protected RoutesController ControllerUnderTest { get; }
        protected IRouteService RouteServiceFake { get; } 

        public RoutesControllerTests()
        {
            RouteServiceFake = A.Fake<IRouteService>();
            ControllerUnderTest = new RoutesController(RouteServiceFake);
        }

        public class ReadAll : RoutesControllerTests
        {
            [Fact]
            public async Task ShouldReturnOk_ObjectResultWithRoutes()
            {
                // Arrange
                var expectedRoutes = new List<Route>
                {
                    new Route(1, new List<Point>())
                };
                A.CallTo(() => RouteServiceFake.ReadAll()).Returns(expectedRoutes);

                // Act
                var result = await ControllerUnderTest.ReadAll();

                // Assert
                var okResult = Assert.IsType<OkObjectResult>(result);
                Assert.Same(expectedRoutes, okResult.Value);
            }
        }

        public class ReadOne : RoutesControllerTests
        {
            [Fact]
            public async void ShouldReturnNotFoundResult_WhenRouteNotFoundExceptionIsThrown()
            {
                // Arrange
                A.CallTo(() => RouteServiceFake.ReadOne(1)).Throws(() => new RouteNotFoundException());

                // Act
                var result = await ControllerUnderTest.ReadOne(1);

                // Assert
                Assert.IsType<NotFoundObjectResult>(result);
            }
        }
    }
}