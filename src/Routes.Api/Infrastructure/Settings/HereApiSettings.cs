﻿namespace Routes.Api.Infrastructure.Settings
{
    public class HereApiSettings
    {
        public string AppId { get; set; }
        public string AppCode { get; set; }
        public string BaseAddress { get; set; }
    }
}
