﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Routes.Api.Infrastructure.Models;
using Routes.Api.Infrastructure.RouteConverter;
using Routes.Api.Infrastructure.Settings;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Routes.Api.Infrastructure.Services
{
    public class HereApiClient : IHereApiClient
    {
        private readonly IRouteConverter _routeToGPXConverter;
        private readonly IOptions<HereApiSettings> _hereApiSettings;
        private HttpClient _client;

        public HereApiClient(HttpClient client, IOptions<HereApiSettings> hereApiSettings, IRouteConverter routeToGPXConverter)
        {
            _routeToGPXConverter = routeToGPXConverter ?? throw new ArgumentNullException(nameof(routeToGPXConverter));
            _hereApiSettings = hereApiSettings ?? throw new ArgumentNullException(nameof(hereApiSettings));

            _client = client;
            _client.BaseAddress = new Uri(_hereApiSettings.Value.BaseAddress);
            _client.DefaultRequestHeaders.Clear();
        }

        public async Task<MatchedRoute> MatchRoute(Route routeToMatch)
        {
            var parameters =$"?app_id={_hereApiSettings.Value.AppId}&app_code={_hereApiSettings.Value.AppCode}";
            var request = new HttpRequestMessage(HttpMethod.Post, parameters);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var route = _routeToGPXConverter.Convert(routeToMatch);
            var byteArray = Encoding.ASCII.GetBytes(route);
            request.Content = new ByteArrayContent(byteArray);
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/binary");

            using (var response = await _client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead))
            {
                response.EnsureSuccessStatusCode();
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<MatchedRoute>(content);
            }
        }
    }
}