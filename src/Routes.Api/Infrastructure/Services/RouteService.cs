﻿using AutoMapper;
using Routes.Api.Infrastructure.Exceptions;
using Routes.Api.Infrastructure.Models;
using Routes.Api.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Routes.Api.Infrastructure.Services
{
    public class RouteService : IRouteService
    {
        private readonly IRouteRepository _routeRepository;
        private readonly IHereApiClient _hereApiClient;
        private readonly IMapper _mapper;

        public RouteService(IRouteRepository routeRepository, IHereApiClient hereApiClient, IMapper mapper)
        {
            _routeRepository = routeRepository ?? throw new ArgumentNullException(nameof(routeRepository));
            _hereApiClient = hereApiClient ?? throw new ArgumentNullException(nameof(hereApiClient));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public IEnumerable<Route> ReadAll()
        {
            return _routeRepository.ReadAll();
        }

        public async Task<IEnumerable<Route>> ReadAllWithRouteMatching()
        {
            var mismatchedRoutes = _routeRepository.ReadAll();
            var matchedRoutes = await Task.WhenAll(mismatchedRoutes.Select(route => _hereApiClient.MatchRoute(route)));
            return _mapper.Map<IEnumerable<Route>>(matchedRoutes);
        }

        public Route ReadOne(int id)
        {
            var route = _routeRepository.ReadOne(id);
            if (route == null)
                throw new RouteNotFoundException();

            return route;
        }

        public async Task<Route> ReadOneWithRouteMatching(int id)
        {
            var mismatchedRoute = _routeRepository.ReadOne(id);
            if (mismatchedRoute == null)
                throw new RouteNotFoundException();

            var matchedRoute = await _hereApiClient.MatchRoute(mismatchedRoute);
            return _mapper.Map<Route>(matchedRoute);

        }
    }
}
