﻿using Routes.Api.Infrastructure.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Routes.Api.Infrastructure.Services
{
    public interface IRouteService
    {
        IEnumerable<Route> ReadAll();
        Task<IEnumerable<Route>> ReadAllWithRouteMatching();
        Task<Route> ReadOneWithRouteMatching(int id);
        Route ReadOne(int id);
    }
}
