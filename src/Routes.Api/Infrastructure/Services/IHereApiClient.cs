﻿using Routes.Api.Infrastructure.Models;
using System.Threading.Tasks;

namespace Routes.Api.Infrastructure.Services
{
    public interface IHereApiClient
    {
        Task<MatchedRoute> MatchRoute(Route routeToMatch);
    }
}