﻿using Routes.Api.Infrastructure.Models;

namespace Routes.Api.Infrastructure.RouteConverter
{
    public interface IRouteConverter
    {
        string Convert(Route route);
    }
}
