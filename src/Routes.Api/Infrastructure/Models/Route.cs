﻿using System.Collections.Generic;

namespace Routes.Api.Infrastructure.Models
{
    public class Route
    {
        public int Id { get; set; }
        public IEnumerable<Point> Points { get; set; }
        private Route(){}
        public Route(int id, IEnumerable<Point> points)
        {
            this.Id = id;
            this.Points = points;
        }
    }
}
