﻿using Newtonsoft.Json;

namespace Routes.Api.Infrastructure.Serialization
{
    public class NewtonsoftJsonSerializer : IJsonSerializer
    {
        public T Deserialize<T>(string jsonToDeserialize)
        {
            return JsonConvert.DeserializeObject<T>(jsonToDeserialize);
        }
    }
}
