﻿namespace Routes.Api.Infrastructure.Serialization
{
    public interface IJsonSerializer
    {
        T Deserialize<T>(string jsonToDeserialize);
    }
}
