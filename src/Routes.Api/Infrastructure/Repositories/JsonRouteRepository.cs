﻿using Microsoft.AspNetCore.Hosting;
using Routes.Api.Infrastructure.Exceptions;
using Routes.Api.Infrastructure.Models;
using Routes.Api.Infrastructure.Serialization;
using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;

namespace Routes.Api.Infrastructure.Repositories
{
    public class JsonRouteRepository : IRouteRepository
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IFileSystem _fileSystem;
        private readonly IJsonSerializer _jsonSerializer;

        public JsonRouteRepository(IHostingEnvironment hostingEnvironment, IFileSystem fileSystem, IJsonSerializer jsonSerializer)
        {
            _hostingEnvironment = hostingEnvironment ?? throw new ArgumentNullException(nameof(hostingEnvironment));
            _fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));
            _jsonSerializer = jsonSerializer ?? throw new ArgumentNullException(nameof(jsonSerializer));
        }

        public IEnumerable<Route> ReadAll()
        {
            var pointsFiles = GetAllPointsFiles();
            var routes = pointsFiles.Select(file => new Route(file.FileId, _jsonSerializer.Deserialize<List<Point>>(file.Content)));

            return routes;
        }


        public Route ReadOne(int id)
        {
            var pointsFile = GetAllPointsFiles().FirstOrDefault(pf => pf.FileId == id);
            if (pointsFile == null)
                throw new RouteNotFoundException();

            return new Route(pointsFile.FileId, _jsonSerializer.Deserialize<List<Point>>(pointsFile.Content));
        }

        private IEnumerable<PointsFile> GetAllPointsFiles()
        {
            string contentRootPath = _hostingEnvironment.ContentRootPath;

            return _fileSystem.Directory
                              .GetFiles($"{contentRootPath}/PointsData")
                              .Select(f => new PointsFile
                              {
                                  FileId = GetPointsFileOrdinalNumber(f),
                                  Content = _fileSystem.File.ReadAllText(f)
                              });
        }

        private int GetPointsFileOrdinalNumber(string pointsFilePath)
        {
            return (int)char.GetNumericValue(_fileSystem.Path.GetFileName(pointsFilePath).First());
        }
    }
}