﻿using Routes.Api.Infrastructure.Models;
using System.Collections.Generic;

namespace Routes.Api.Infrastructure.Repositories
{
    public interface IRouteRepository
    {
        IEnumerable<Route> ReadAll();
        Route ReadOne(int id);
    }
}
